# Git repo as package repo

[[_TOC_]]

You can use a git repository as a private (or public) NPM package repository.

Let's assume that `https://gitlab.com/daverona/templates/node.git` is the git repository of interest.

## Quick Start

### Listing

To list all tags, branches, or both:

```bash
# using SSH
git ls-remote --tags --sort="-version:refname" ssh://git@gitlab.com/daverona/templates/node.git   # tags only
git ls-remote --heads --sort="-version:refname" ssh://git@gitlab.com/daverona/templates/node.git  # branches only
git ls-remote --refs --sort="-version:refname" ssh://git@gitlab.com/daverona/templates/node.git   # both
# using HTTPS
git ls-remote --tags --sort="-version:refname" https://gitlab.com/daverona/templates/node.git   # tags only
git ls-remote --heads --sort="-version:refname" https://gitlab.com/daverona/templates/node.git  # branches only
git ls-remote --refs --sort="-version:refname" https://gitlab.com/daverona/templates/node.git   # both
```

### Installation

To install a specific reference (tag or branch) or commit hash, say `ref`:

```bash
# using SSH
npm install git+ssh://git@gitlab.com/daverona/templates/node.git#ref  
# using HTTPS
npm install git+https://gitlab.com/daverona/templates/node.git#ref    
```

> Note that 
the last commit on master branch will be installed if `#ref` is omitted
and a short hash (the first 7 characters of a commit hash) may be used instead of a commit hash.

## References

* git ls-remote: [https://git-scm.com/docs/git-ls-remote.html](https://git-scm.com/docs/git-ls-remote.html)
* Use Github branch as dependency in package.json: [https://medium.com/@jonchurch/use-github-branch-as-dependency-in-package-json-5eb609c81f1a](https://medium.com/@jonchurch/use-github-branch-as-dependency-in-package-json-5eb609c81f1a)
* [https://stackoverflow.com/a/27015550](https://stackoverflow.com/a/27015550)

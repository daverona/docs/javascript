# JavaScript

[[_TOC_]]

## Tutorials

* JavaScript Tutorial For Beginners: [https://youtu.be/qoSksQ4s_hg](https://youtu.be/qoSksQ4s_hg)
* Modern JavaScript Tutorial: [https://youtu.be/iWOYAxlnaww](https://youtu.be/iWOYAxlnaww)
* JavaScript ES6 Tutorial: [https://youtu.be/0Mp2kwE8xY0](https://youtu.be/0Mp2kwE8xY0)
* Object Oriented JavaScript: [https://youtu.be/4l3bTDlT6ZI](https://youtu.be/4l3bTDlT6ZI)
* A mostly complete guide to webpack 5 (2020): [https://www.valentinog.com/blog/webpack/](https://www.valentinog.com/blog/webpack/)
* webpack Tutorial: How to Set Up webpack 5 From Scratch: [https://www.taniarascia.com/how-to-use-webpack/](https://www.taniarascia.com/how-to-use-webpack/)

## Style Guides

* Google JavaScript Style Guide: [https://google.github.io/styleguide/jsguide.html](https://google.github.io/styleguide/jsguide.html)

## Documentation

* The Modern JavaScript Tutorial: [https://javascript.info/](https://javascript.info/)
* JavaScript Guide: [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide)
* npm: [https://docs.npmjs.com/](https://docs.npmjs.com/)
* yarn: [https://yarnpkg.com/getting-started](https://yarnpkg.com/getting-started)
* webpack: [https://webpack.js.org/concepts/](https://webpack.js.org/concepts/)
* Creating Node.js modules: [https://docs.npmjs.com/creating-node-js-modules](https://docs.npmjs.com/creating-node-js-modules)
* Understanding "This" in JavaScript: [https://www.codementor.io/@dariogarciamoya/understanding--this--in-javascript-du1084lyn](https://www.codementor.io/@dariogarciamoya/understanding--this--in-javascript-du1084lyn)
* Understanding "this" in javascript with arrow functions: [https://www.codementor.io/@dariogarciamoya/understanding-this-in-javascript-with-arrow-functions-gcpjwfyuc](https://www.codementor.io/@dariogarciamoya/understanding-this-in-javascript-with-arrow-functions-gcpjwfyuc)

## Linters

* ESLint: [https://eslint.org/](https://eslint.org/)
* Prettier: [https://prettier.io/](https://prettier.io/)

## WebStorm

* Configuring JavaScript debugger: [https://www.jetbrains.com/help/webstorm/configuring-javascript-debugger.html](https://www.jetbrains.com/help/webstorm/configuring-javascript-debugger.html)
* Debugging JavaScript Deployed to a Remote Server: [https://www.jetbrains.com/help/webstorm/debugging-javascript-on-an-external-server-with-mappings.html](https://www.jetbrains.com/help/webstorm/debugging-javascript-on-an-external-server-with-mappings.html)

## Packages

* axios: [https://www.npmjs.com/package/axios](https://www.npmjs.com/package/axios)
* ccapture.js: [https://www.npmjs.com/package/ccapture.js](https://www.npmjs.com/package/ccapture.js)
* chalk: [https://www.npmjs.com/package/chalk](https://www.npmjs.com/package/chalk)
* collect.js: [https://www.npmjs.com/package/collect.js](https://www.npmjs.com/package/collect.js)
* commander: [https://www.npmjs.com/package/commander](https://www.npmjs.com/package/commander)
* ~~date-fns: [https://www.npmjs.com/package/date-fns](https://www.npmjs.com/package/date-fns)~~
* debug: [https://www.npmjs.com/package/debug](https://www.npmjs.com/package/debug)
* decode-tiff: [https://www.npmjs.com/package/decode-tiff](https://www.npmjs.com/package/decode-tiff)
* dot-object: [https://www.npmjs.com/package/dot-object](https://www.npmjs.com/package/dot-object)
* dot-prot: [https://www.npmjs.com/package/dot-prop](https://www.npmjs.com/package/dot-prop)
* jquery: [https://www.npmjs.com/package/jquery](https://www.npmjs.com/package/jquery)
* libphonenumber-js: [https://www.npmjs.com/package/libphonenumber-js](https://www.npmjs.com/package/libphonenumber-js)
* lodash: [https://www.npmjs.com/package/lodash](https://www.npmjs.com/package/lodash)
* moment: [https://www.npmjs.com/package/moment](https://www.npmjs.com/package/moment)
* pngjs: [https://www.npmjs.com/package/pngjs](https://www.npmjs.com/package/pngjs)
* promise-worker: [https://www.npmjs.com/package/promise-worker](https://www.npmjs.com/package/promise-worker)
* queue: [https://www.npmjs.com/package/queue](https://www.npmjs.com/package/queue)
* webfontloader: [https://www.npmjs.com/package/webfontloader](https://www.npmjs.com/package/webfontloader)
* worker-loader: [https://www.npmjs.com/package/worker-loader](https://www.npmjs.com/package/worker-loader)

### Development only

* babel:
* eslint: [https://www.npmjs.com/package/eslint](https://www.npmjs.com/package/eslint)

## References

* JavaScript ES6 Tutorial: A Complete Crash Course on Modern JS: [https://medium.com/javascript-in-plain-english/javascript-es6-tutorial-a-complete-crash-course-on-modern-js-a09294bffdb7](https://medium.com/javascript-in-plain-english/javascript-es6-tutorial-a-complete-crash-course-on-modern-js-a09294bffdb7)
* JavaScript brief history and ECMAScript(ES6,ES7,ES8,ES9) features: [https://medium.com/@madasamy/javascript-brief-history-and-ecmascript-es6-es7-es8-features-673973394df4](https://medium.com/@madasamy/javascript-brief-history-and-ecmascript-es6-es7-es8-features-673973394df4)

# Vue.js

[[_TOC_]]

## Tutorials

* Vue.js 2 Tutorial: [https://youtu.be/5LYrN_cAJoA](https://youtu.be/5LYrN_cAJoA)
* Vue CLI V3 Tutorial: [https://youtu.be/KeFdy1kVH4A](https://youtu.be/KeFdy1kVH4A)
* Vuex Tutorial: [https://youtu.be/BGAu__J4xoc](https://youtu.be/BGAu__J4xoc)
* Vuetify Tutorial: [https://youtu.be/2uZYKcKHgU0](https://youtu.be/2uZYKcKHgU0)
* Django Vue Tutorial: [https://www.youtube.com/playlist?list=PLhi_KT82zuWaKZhV_0w67F2UXJqFareXz](https://www.youtube.com/playlist?list=PLhi_KT82zuWaKZhV_0w67F2UXJqFareXz)

## Courses

* Vue.js & JavaScript Courses: [https://vueschool.io/courses?filter=free-courses](https://vueschool.io/courses?filter=free-courses)

## Documentation

* Vue.js Guide: [https://vuejs.org/v2/guide/](https://vuejs.org/v2/guide/)
* Vue.js API Reference: [https://vuejs.org/v2/api/](https://vuejs.org/v2/api/)
* Vue Router Guide: [https://router.vuejs.org/](https://router.vuejs.org/)
* Vue Router API Reference: [https://router.vuejs.org/api/](https://router.vuejs.org/api/)
* Vuex Guide: [https://vuex.vuejs.org/guide/](https://vuex.vuejs.org/guide/)
* Vuex API Reference: [https://vuex.vuejs.org/api/](https://vuex.vuejs.org/api/)
* Vue SSR Guide: [https://ssr.vuejs.org/](https://ssr.vuejs.org/)

## Style Guides

* Vue Style Guide: [https://vuejs.org/v2/style-guide/](https://vuejs.org/v2/style-guide/)

## WebStorm

* Working with Vue.js in WebStorm: [https://blog.jetbrains.com/webstorm/2018/01/working-with-vue-js-in-webstorm/](https://blog.jetbrains.com/webstorm/2018/01/working-with-vue-js-in-webstorm/)

## Packages

* ~~element-ui: [https://www.npmjs.com/package/element-ui](https://www.npmjs.com/package/element-ui)~~
* vee-validate: [https://www.npmjs.com/package/vee-validate](https://www.npmjs.com/package/vee-validate)
* vue: [https://www.npmjs.com/package/vue](https://www.npmjs.com/package/vue)
* vue-clipboard2: [https://www.npmjs.com/package/vue-clipboard2](https://www.npmjs.com/package/vue-clipboard2)
* vue-cookies: [https://www.npmjs.com/package/vue-cookies](https://www.npmjs.com/package/vue-cookies)
* vue-gtag: [https://www.npmjs.com/package/vue-gtag](https://www.npmjs.com/package/vue-gtag)
* vue-i18n: [https://www.npmjs.com/package/vue-i18n](https://www.npmjs.com/package/vue-i18n)
* vue-loader: [https://vue-loader.vuejs.org/](https://vue-loader.vuejs.org/)
* vue-meta: [https://www.npmjs.com/package/vue-meta](https://www.npmjs.com/package/vue-meta)
* vue-recaptcha: [https://www.npmjs.com/package/vue-recaptcha](https://www.npmjs.com/package/vue-recaptcha)
* vue-router: [https://www.npmjs.com/package/vue-router](https://www.npmjs.com/package/vue-router)
* vue-scrollto: [https://www.npmjs.com/package/vue-scrollto](https://www.npmjs.com/package/vue-scrollto)
* vue-session: [https://www.npmjs.com/package/vue-session](https://www.npmjs.com/package/vue-session)
* vue-upload-component: [https://www.npmjs.com/package/vue-upload-component](https://www.npmjs.com/package/vue-upload-component)
* vuetify: [https://www.npmjs.com/package/vuetify](https://www.npmjs.com/package/vuetify)
* vuex: [https://www.npmjs.com/package/vuex](https://www.npmjs.com/package/vuex)
* vuex-pathify: [https://www.npmjs.com/package/vuex-pathify](https://www.npmjs.com/package/vuex-pathify)
* vuex-router-sync: [https://www.npmjs.com/package/vuex-router-sync](https://www.npmjs.com/package/vuex-router-sync)
* vue-snotify: 
* vue2-scrollspy:
* vuex-persistedstate:
* vuedraggable:
* vue-resize-directive:
* vue-horizontal:

## References

* 4 Best Practices for Large Scale Vue.js Projects: [https://blog.bitsrc.io/4-best-practices-for-large-scale-vue-js-projects-9a533450bdb2](https://blog.bitsrc.io/4-best-practices-for-large-scale-vue-js-projects-9a533450bdb2)
* 10 Good Practices for Building and Maintaining Large Vue.js Projects: [https://www.telerik.com/blogs/10-good-practices-building-maintaining-large-vuejs-projects](https://www.telerik.com/blogs/10-good-practices-building-maintaining-large-vuejs-projects)
* Vue.js Login Component with Spinner: [https://webdevchallenges.com/vue-js-login-component-with-spinner/](https://webdevchallenges.com/vue-js-login-component-with-spinner/)
* Use Protected Routes in Vue.js: [https://webdevchallenges.com/use-protected-routes-in-vue-js/](https://webdevchallenges.com/use-protected-routes-in-vue-js/)
* Authentication best practices for Vue: [https://blog.sqreen.com/authentication-best-practices-vue/](https://blog.sqreen.com/authentication-best-practices-vue/)
* GitHub netflix/mantis-ui repository: [https://github.com/Netflix/mantis-ui](https://github.com/Netflix/mantis-ui)
* GitHub gothinkster/vue-realworld-example-app repository: [https://github.com/gothinkster/vue-realworld-example-app](https://github.com/gothinkster/vue-realworld-example-app)
* GitHub vuetifyjs/vuetify repository: [https://github.com/vuetifyjs/vuetify](https://github.com/vuetifyjs/vuetify)
* Pre-made layouts: [https://v2.vuetifyjs.com/en/getting-started/pre-made-layouts/](https://v2.vuetifyjs.com/en/getting-started/pre-made-layouts/)
* Pre-made layouts GitHub repository: [https://github.com/vuetifyjs/vuetify/tree/v2.3.10/packages/docs/src/layouts/layouts/demos](https://github.com/vuetifyjs/vuetify/tree/v2.3.10/packages/docs/src/layouts/layouts/demos)
* Vuetify docs source: [https://github.com/vuetifyjs/vuetify/tree/v2.4.2/packages/docs](https://github.com/vuetifyjs/vuetify/tree/v2.4.2/packages/docs)

<!--
## Django with Vue(tify)

### Building a modern web application with Django REST framework and Vue: 
Building views and REST API

* [https://www.techiediaries.com/django-vuejs-api-views/](https://www.techiediaries.com/django-vuejs-api-views/)
* [https://www.techiediaries.com/django-vuejs-auth0/](https://www.techiediaries.com/django-vuejs-auth0/)
* Repository: [https://github.com/techiediaries/django-auth0-vue](https://github.com/techiediaries/django-auth0-vue)


### Integrating Django and VueJs with Vue CLI 3 and Webpack Loader (unfinished)

* Part 1: [https://medium.com/@rodrigosmaniotto/integrating-django-and-vuejs-with-vue-cli-3-and-webpack-loader-145c3b98501a](https://medium.com/@rodrigosmaniotto/integrating-django-and-vuejs-with-vue-cli-3-and-webpack-loader-145c3b98501a)
* Repository: [https://github.com/ottosmaniotto/django-vuecli3](https://github.com/ottosmaniotto/django-vuecli3)

### Simple Movies web app with Vue, Vuetify and django (unfinished series)

* 1. Part 1: Setup [https://medium.com/@samy_raps/simple-movies-web-app-with-vue-vuetify-and-django-part-1-setup-6351c02327a5](https://medium.com/@samy_raps/simple-movies-web-app-with-vue-vuetify-and-django-part-1-setup-6351c02327a5)
* 2. Part 2: Login [https://medium.com/@samy_raps/simple-movies-web-app-with-vue-vuetify-and-django-part-2-login-2414b984500b](https://medium.com/@samy_raps/simple-movies-web-app-with-vue-vuetify-and-django-part-2-login-2414b984500b)
* 3. Part 3: The API [https://medium.com/@samy_raps/simple-movies-web-app-with-vue-vuetify-and-django-part-3-the-api-53113837dbce](https://medium.com/@samy_raps/simple-movies-web-app-with-vue-vuetify-and-django-part-3-the-api-53113837dbce)

### Cute cats web app (django + VueJs)

* 1. Settings: [https://medium.com/@jrmybrcf/how-to-build-api-rest-project-with-django-vuejs-part-i-228cbed4ce0c](https://medium.com/@jrmybrcf/how-to-build-api-rest-project-with-django-vuejs-part-i-228cbed4ce0c)
* 2. Create VueJS views: [https://medium.com/@jrmybrcf/how-to-build-a-project-with-django-vuejs-custom-frontend-part-273e7ea8ce21](https://medium.com/@jrmybrcf/how-to-build-a-project-with-django-vuejs-custom-frontend-part-273e7ea8ce21)
* 3. Create a REST API with DRF: [https://medium.com/@jrmybrcf/how-to-build-a-project-with-django-vuejs-create-a-rest-api-endpoint-b57374a89661](https://medium.com/@jrmybrcf/how-to-build-a-project-with-django-vuejs-create-a-rest-api-endpoint-b57374a89661)
* 4. Get data from API: [https://medium.com/@jrmybrcf/how-to-build-a-project-with-django-vuejs-get-datas-from-api-with-vuejs-6b4f1eaced6d](https://medium.com/@jrmybrcf/how-to-build-a-project-with-django-vuejs-get-datas-from-api-with-vuejs-6b4f1eaced6d)

### Vue.js In A Django Template
[https://vsupalov.com/vue-js-in-django-template/](https://vsupalov.com/vue-js-in-django-template/)

### django-postgres-vue-gitlab-ecs
[https://gitlab.com/verbose-equals-true/django-postgres-vue-gitlab-ecs](https://gitlab.com/verbose-equals-true/django-postgres-vue-gitlab-ecs)

[https://www.freecodecamp.org/news/how-to-build-a-single-page-application-using-vue-js-vuex-vuetify-and-firebase-838b40721a07/](https://www.freecodecamp.org/news/how-to-build-a-single-page-application-using-vue-js-vuex-vuetify-and-firebase-838b40721a07/)

[https://github.com/gtalarico/django-vue-template](https://github.com/gtalarico/django-vue-template)

[https://github.com/vuejs/vuex/tree/dev/examples](https://github.com/vuejs/vuex/tree/dev/examples)
-->

